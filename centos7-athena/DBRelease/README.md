ALRB and DBRelease Image Configuration
======================================

This configuration can be used to add ALRB and DBRelease to an image with a pre-existing
standalone installation of Athena. 

To add a layer with ALRB and DBRelease to a pre-existing numbered release image:

```bash
docker build -t <username>/athena:22.0.5_2019-09-24T2128_31.8.1 --build-arg BASEIMAGE=<username>/athena:22.0.5_2019-09-24T2128 --build-arg DBRELEASE=31.8.1 .
```

Additional Setup
======================================

The following environment variable needs to be exported manually, and include the local squid servers (if any)

FRONTIER_SERVER

For standalone mode (no cvmfs)

unset FRONTIER_SERVER
