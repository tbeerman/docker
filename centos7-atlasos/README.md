CC7 ATLAS OS
=============

This configuration can be used to set up a base image that ATLAS analysis
and offline releases can be installed on top of. So that each image would
not have to duplicate the same base components.

You can build it like:

```bash
docker build -t atlas/centos7-atlasos:X.Y.Z -t atlas/centos7-atlasos:latest \
   --compress --squash .
```

Compiler
--------

Note that the image holds GCC 8.3.0 out of the box. This is because **all**
production mode ATLAS releases use this compiler at the moment.

Examples
--------

You can find some pre-built images on
[atlas/centos7-atlasos](https://hub.docker.com/r/atlas/centos7-atlasos/).
